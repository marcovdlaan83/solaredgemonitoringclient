# Solaredge Power Generation Chart
This image will create a barchart containing your weekly solar power production. 
It will output a static PNG image that you can use on your website or other online profiles.

The image will use the API by SolarEdge based on this documentation: https://www.solaredge.com/sites/default/files/se_monitoring_api.pdf

## Requirements:
First create an API Key in the SolarEdge portal. This key will be used inside the container to request your generation statistics.

## run 
```
docker run -d -p 7070:80-e SOLAREDGE_APIKEY=KEY -e SOLAREDGE_SITEID=SiteId -e CACHE_MINUTES=15 enterz1983/solaredgemonitoringclient
```
Go to the browser on the specified port and open : http://yoursite:7070/weekly?h=400&w=800

## Technical details:
Using the default aspnet core image with an addition of libgdiplus for rendering the chart.
For the chart I make use of ScottPlot : http://swharden.com/scottplot/

## State:
Since I wrote all this in a hurry, it's a bit messy. Might add more features later and will give access to git repro for sourcecode.

## Limitations:
No https support, so better use a reverse proxy that terminates your SSL connection.
Also the SolarEdge API has a limit of 300 requests per day, so uses caching for image output as well as data gathering.

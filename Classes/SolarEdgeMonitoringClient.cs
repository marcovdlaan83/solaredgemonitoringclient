namespace SolarEdgeClient.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Newtonsoft.Json.Linq;
    using System.Runtime.Caching;
    

    public class SolarEdgeMonitoringClient
    {
        private string _apiKey;
        private int _siteId;
        private int _cacheMinutes;
        const string baseUrl = "https://monitoringapi.solaredge.com/";
        const string weeklyDataKey = "solaredgeWeeklyData";
        public SolarEdgeMonitoringClient(string apiKey, int siteId, int cacheMinutes){
            this._apiKey = apiKey;
            this._siteId = siteId;
            this._cacheMinutes = cacheMinutes;
        }

        public List<DailyProductionData> GetPreviousWeekData(bool forceRenew = false){
            ObjectCache cache = MemoryCache.Default;
            List<DailyProductionData> data;
            data = cache[weeklyDataKey] as List<DailyProductionData>;

            if (data == null){
                data = new List<DailyProductionData>();
                HttpClient http = new HttpClient();
                string startDate = DateTime.Now.AddDays(-6).ToString("yyyy-MM-dd");
                string endDate = DateTime.Now.ToString("yyyy-MM-dd");

                string url = String.Format("{0}/site/{1}/energy?timeUnit=DAY&endDate={2}&startDate={3}&api_key={4}", baseUrl, _siteId, endDate, startDate, _apiKey);
                var responseData = http.GetAsync(url).Result.Content.ReadAsStringAsync().Result;
                var energyProductionData = EnergyProductionData.FromJson(responseData);
                foreach(Value v in energyProductionData.Energy.Values){
                    data.Add(new DailyProductionData(DateTime.Parse(v.Date), Convert.ToInt32(v.ValueValue)));
                }
                
                cache.Set(weeklyDataKey, data, DateTimeOffset.Now.AddMinutes(_cacheMinutes));
            }

            return data;
        }
    }

    public struct DailyProductionData
    {
        public DailyProductionData(DateTime logdate, int wh){
            Logdate = logdate;
            Wh = wh;
        }

        public DateTime Logdate { get; }
        public int Wh { get; }
    }
}
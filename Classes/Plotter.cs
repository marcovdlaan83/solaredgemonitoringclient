using System.Collections.Generic;
using System.Linq;
using ScottPlot;
using System.IO;
using System;

namespace SolarEdgeClient.Classes
{
    public class Plotter
    {
        const string WEEKLYCHARTFILENAME = "output/weekly";
        public Plotter(){
            
        }

        public static string GetWeeklyGraph(List<DailyProductionData> data, int cacheMinutes, bool forceRenew=false, int width=600, int height=400){
            string filename = string.Format("{0}_{1}_{2}.png",WEEKLYCHARTFILENAME, width, height);
            FileInfo fi = new FileInfo(filename);
            if(!fi.Exists || 
                forceRenew || 
                DateTime.Now > fi.LastWriteTime.AddMinutes(cacheMinutes))
                {
                    Plotter.PlotWeeklyGraph(data, filename, width, height);
                }
            
            return fi.FullName;
        }

        public static void PlotWeeklyGraph(List<DailyProductionData> data, string filename, int width=600, int height=400){
            var plt = new ScottPlot.Plot(width, height);
            // generate random data to plot
            int pointCount = data.Count;
            List<double> doubleList = new List<double>(data.Count);
            data.ForEach(i => doubleList.Add(i.Wh));
            double[] xs = DataGen.Consecutive(pointCount);
            double[] ys = doubleList.ToArray();
            
            // make the bar plot
            plt.PlotBar(xs, ys);

            // customize the plot to make it look nicer
            plt.Axis(y1: 0);
            plt.Grid(enableVertical: false, lineStyle: LineStyle.Dot);
            plt.YLabel("Wh");

            // apply custom axis tick labels
            var days = data.Select(e => e.Logdate.ToString("dd-MM")).ToArray();
            plt.XTicks(xs, days);

            plt.SaveFig(filename);

        }
    }
}